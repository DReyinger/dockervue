# Vue in Docker
Build docker image with:
```
docker build -t vue:0.1.
```

Run following command to install dependencies:
```
docker run -v $(pwd):/src -p 80:80 --rm -it vue:0.1 yarn install
```

Run the following command to start vue app in docker container:
```
docker run -v $(pwd):/src -p 80:80 --rm -it vue:0.1 env HOST="0.0.0.0" env PORT="80" yarn serve
```

