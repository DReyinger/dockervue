FROM node:11.9

LABEL version="0.1"

# update system repositories
RUN ["apt-get", "update"]
RUN ["apt-get", "upgrade", "-y"]
RUN ["apt-get", "autoclean"]

# install apt-utils
RUN ["apt-get", "install", "-y", "--no-install-recommends", "apt-utils"]

# install vim
RUN ["apt-get", "install", "-y", "vim"]

# install vue 
RUN ["npm", "install", "-g", "vue"]
RUN ["npm", "install", "-g", "@vue/cli"]

# prepare project dir
RUN ["mkdir", "/src"]

# define location after login to be src directory
WORKDIR /src

# expose web ports
EXPOSE 80:80
 
